interface IBroadcast {
    start: Date;
    end: Date;
    targetUsers: number;
    usersReceived: number;
}